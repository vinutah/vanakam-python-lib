import numpy as np

arr = np.random.randn(100)

# number of positive values
print((arr > 0).sum())

bools = np.array([False, False, True, False])
print(bools.any())
print(bools.all())

