import numpy as np

arr = np.random.randn(8)
print(arr)

arr.sort()
print(arr)

"""
multidimensional arrays can have each 1d section
of values sorted in-place along an axis
by passing the axis number to sort
"""

arr = np.random.randn(5, 3)
print(arr)

arr.sort(1)
print(arr)

large_arr = np.random.randn(1000)
large_arr.sort()

# 5% Quantile
five_q = large_arr[int(0.05 * len(large_arr))]
print(five_q)
