
"""
Python for Data Analysis by Wes McKinney
Chapter 4: NumPy basics: Arrays and Vectorized Computation
"""

import numpy as np


# expressing conditional logic as list comprehensions

xarr = np.array([1.1, 1.2, 1.3, 1.4, 1.5])
yarr = np.array([2.1, 2.2, 2.3, 2.4, 2.5])
cond = np.array([True, False, True, True, False])
print("{0}\n{1}\n{2}".format(xarr, yarr, cond))
result = [(x if c else y)
          for x, y, c in zip(xarr, yarr, cond)]
print("{}".format(result))

# expressing conditional logic as array operations

result = np.where(cond, xarr, yarr)
print("{}".format(result))
