"""
* Matplotlib
    * We know how to load data, now let's look at it.
    * Line chart, scatter plot, histogram
    * Plotting images
    * 99% of the time, we seem to be doing one of the above
"""

import pylab
import numpy as np
import matplotlib.pyplot as plt

plt.plot(np.arange(10))

pylab.show()

