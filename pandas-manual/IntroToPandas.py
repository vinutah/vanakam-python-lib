"""
pandas
    * primary library of interest ? for who

    * goal is to make data analysis in python

        * fast

        * easy

    * built on top of Numpy

        * easy to use in Numpy-centric applications

    * contains

        * high level data structures

        * manipulation tools

    * design requirements placed on pandas

        * data structures with labeled axes

        * automatic or explicit data alignment

            * prevents common error with mis-aligned data
            * working with differently-indexed data coming from different sources

        * integrated time series analysis
"""

