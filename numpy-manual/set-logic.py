"""
numpy has basic set operations for one-dimensional ndarrays

"""

import numpy as np
names = np.array(['Bob', 'Joe', 'Will', 'Bob', 'Will', 'Joe', 'Joe'])
print(names)
print(np.unique(names))
print(sorted(set(names)))

"""
test membership of the values in one array
in another returning a boolean array
"""
values = np.array([6, 0, 0, 3, 2, 5, 6])
print("values = {}, mem = {}".format(values, np.in1d(values, [2, 3, 6])))

"""
There are more
Set Logic functions

np.unique()
np.intersect1d(x, y)
union1d(x, y)
in1d(x, y)
setdiff1d(x, y)
setxor1d(x, y)

"""