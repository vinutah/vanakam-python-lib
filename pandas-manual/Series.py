from pandas import Series, DataFrame
import pandas as pd

obj = Series([4, 7, -5, 3])

print(f'{obj}', {type(obj)})

print(f'{obj.values}')

print(f'{obj.index}')

obj2 = Series([4, 7, -5, 3], index=['d', 'b', 'a', 'c'])

print(f'{obj2}')

print(f'{obj2.index}')

print("{}".format(obj2['a']))
