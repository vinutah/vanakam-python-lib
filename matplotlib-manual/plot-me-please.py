# how to get a canvas, figure() function

import matplotlib.pyplot as plt
import numpy as np

# create a canvas

my_canvas = plt.figure()

# add sub-plots

ax1 = my_canvas.add_subplot(2, 2, 1)
ax2 = my_canvas.add_subplot(2, 2, 2)
ax3 = my_canvas.add_subplot(2, 2, 3)

# directly plot o

ax1.hist(np.random.standard_normal(100), bins=20, color='k', alpha=0.3)
ax2.scatter(np.arange(30), np.arange(30) + 3 * np.random.standard_normal(30))
ax3.hist(np.random.standard_normal(100), bins=20, color='k', alpha=0.3)

ax4 = my_canvas.add_subplot(2, 2, 4)
plt.plot(np.random.standard_normal(50))

#plt.show()

fig, axes = plt.subplots(2, 3)

print(fig)

print(axes)

