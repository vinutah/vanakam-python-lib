import pylab

import numpy as np

import matplotlib.pyplot as plt

import math

plt.subplot(111)


t = np.arange(0.0, 3.01, 0.01)

s = np.sin(2*math.pi*t)

c = np.sin(4*math.pi*t)

plt.fill(t, s, 'blue', t, c, 'green', alpha=0.3)

plt.title(r'TeX is No. $\sum_{n=1}^\infty \frac{-e^{i\pi}}{2^n}$! ')

pylab.show()