import numpy as np

# 1000 equally spaces points
points = np.arange(-5, 5, 0.01)

xs, ys = np.meshgrid(points, points)

print(ys)
print(xs)