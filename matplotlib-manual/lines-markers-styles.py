import matplotlib.pyplot as plt

import numpy as np

#x = np.random.randn(50)
#y = np.random.randn(50)

x = np.random.randn(30).cumsum()

plt.plot(x, color= 'g', linestyle = '--', label = 'Default')
plt.plot(x, color= 'g', linestyle = '--', label = 'steps-post', drawstyle='steps-post')

plt.savefig('colors-markers-line-styles.png')
