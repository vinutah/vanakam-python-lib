
import numpy as np


"""
ABSTRACT


* MATRIX CREATION
* MATIX INDEXING


* Generating Matrices
* Uniform Distribution
* Normal Distribution

* MATRIX OPERATIONS
* Transpose
* Inverse
* Determinant
* Obtain Diagonal elements
* Inner Product
* Outer Product
* Trace
* Eigen Values
* Eigen Vectors
* LINEAR SYSTEMS

    
"""

#
# Mathematical object
# vector = one-dimensional numpy array
# matrix = two-dimensional numpy array
#

#######################################################

#
# create a two-dimensional numpy array
# think : numpy array as matrix
# think : numpy array as a list of lists
#

"""
MATRIX CREATION
"""

N = np.array([[1,2], [3,4]])
print("{} is of type {}".format(N, type(N)))

# for reference create a lists of lists
L = [[1,2], [3,4]]

#######################################################

"""
MATRIX INDEXING
"""

print('\n')
print("{} is of type {}".format(L, type(L)))

# indexing an array
print("{} is of type {}".format(L[0], type(L[0])))
print('\n')

print("{} is of type {}".format(L[0][0], type(L[0][0])))
print('\n')

print("{} is of type {}".format(N[0][0], type(N[0][0])))
print('\n')

print("{} is of type {}".format(N[0,0], type(N[0][0])))
print('\n')

# not recommended, numpy matrix
N2 = np.matrix([[1, 2],[3, 4]])

# print a numpy matrix
print("{} is a {}".format(N2, type(N2)))
print('\n')

print("convert the numpy matrix to an array")
print('\n')

# convert to an array, why ? for methods ?
A = np.array(N2)

#
# Motivation:
# generating these numpy arrays
# by hand is not scalable
#
# call np.array and pass in a list
# np.array([1, 2, 3])
#

#
# numpy gives us methods for this
#

# one-dimensional
z = np.zeros(10)
print("{} is of type {}".format(z, type(z)))

# two-dimensional
# all zeros ?
# pass a tuple, containing each dimension
z = np.zeros((10, 10))
print("{} is of type {}".format(z, type(z)))

# two-dimensional
# all ones ?
z = np.ones((10, 10))
print("{} is of type {}".format(z, type(z)))

"""
UNIFORM DISTRIBUTION
"""

# all, same is not cool, need a distribution ?
R = np.random.random((10, 10))
print("{} is of type {}".format(R, type(R)))

"""
NORMAL DISTRIBUTION
"""

# requires integers, mean 0 and variance 1
G = np.random.randn(10, 10)
print("{} is of type".format(G, type(G)))

print("the mean of {} is {}".format(G, G.mean()))

G.var()
print("the var of {} is {}".format(G, G.var()))

"""
TRANSPOSE
"""

print("{} is a {}".format(A, type(A)))
print('\n')
print("{} is a transpose of {}".format(A.T,A))

"""
INVERSE
"""

A = np.array([[1, 2],[3, 4]])

Ainv = np.linalg.inv(A)

print(A.dot(Ainv))

print(Ainv.dot(A))

"""
DETERMINANT
"""

print(np.linalg.det(A))


print(np.diag(A))

print(np.diag(A))

a = np.array([1, 2])
print(a)

b = np.array([3, 4])
print(b)

print(np.outer(a, b))

# same as dot product
print(np.inner(a, b))

"""
TRACE
"""

# Sum of the diagonal elements

np.diag(A).sum()

np.trace(A)

"""
EIGEN STUFF
"""

X = np.random.randn(100, 3)

cov = np.cov(X)

print(cov.shape)

cov = np.cov(X.T)

print(cov.shape)

print(cov)

print(np.linalg.eigh(cov))

print(np.linalg.eig(cov))

"""
LINEAR SYSTEM
"""

A = np.array([[1, 2], [3, 4]])
b = np.array([3, 4])
x = np.linalg.inv(A).dot(b)

print("the solution is {}".format(x))

x = np.linalg.solve(A, b)

"""
EXAMPLE
"""
A = np.array([[1.5, 4], [1, 1]])
b = np.array([5050, 2200])
x = np.linalg.solve(A, b)
print("the number of kids     : {} \n"
      "the number of children : {}".format(x[0], x[1]))
