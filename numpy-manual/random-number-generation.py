import numpy as np

"""
numpy.random module
supplements the built-in python random
with functions ofr efficiently generating
whole arrays of sample values
from many kinds of probability distributions.
"""

samples = np.random.normal(size=(4, 4))
print(samples)

# exit(1)

"""
pythons build in random module
samples only one value at a time

np.random is faster for generating 
very large samples
"""

from random import normalvariate

N = 5

samples = [normalvariate(0,1) for _ in range(N)]
print(samples)

print(np.random.normal(1, 2, N))




