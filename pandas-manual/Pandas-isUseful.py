"""
* Pandas
    * Does a lot under the hood
        * load dataset in 1 line
    * A lot like R
    * Central Object: DataFrame
    * Compare data loading with Pandas vs. Manual
    * Operations: filtering by row/col, apply, join
    * Much like SQL
"""

import numpy as np
import pandas as pd

"""
Motivation

* Deep Learning and Machine Learning, we are basically
  learning from data
* Where do we get this data from ?
    * The Internet !
    * Apache Logs
    * Kaggle + Others
* The Internet
    * A lot of data, need a lot of compute
    * Hard Engineering problems :(
    * Its Raw, unstructured data
* Apache Logs + MPI Traces
    * Semi - Structured data
    * The lines you see here will be heterogeneous
* Kaggle + UCI + Any CSV
    * Structured Data sets
    * Usually Comma Separated Files
    * Each record is a Row
    * Each record's value is separated by commas
    * Can view as a table and process in MS Excel
    * Data Scientists --> Convert to Matrix
"""

"""
Example
    * Structured Data 
        * data_2d.csv
        * Some two-dimensional function 
"""

X = []

dataset = '/home/vinu/vanakam-python-lib/pandas/data_2d.csv'

for line in open(dataset):
    row = line.split(',')

    # since we want numerical values
    # cast these strings into floats
    sample = list(map(float, row))

    # growing a list
    # each element is a list
    X.append(sample)


# we now have a list of lists
# convert a lists of lists to a two-dimensional
# numpy array

X = np.array(X)

print("{} is of shape {}".format(X, X.shape))

"""
* Motivation for DataFrames

    * When we first take a look at the Pandas DataFrames
    * We can have only one of 2 reactions, depending
      upon what background we have
        1. Familiar with R
            * Yhea .. Its like a R data frame
        2. Unfamiliar with R
            * Seems backward
            * Contrary to the way Numpy works
    * Keep this in mind when studying Pandas
        * The goal = do not learn everything what pandas can do
        * learn just what we need for ML/Data Science
        * Why ?
        * Because, once we load in data using pandas,
        * we will not use it !
        * we will immediately convert it into a numpy array.
"""

X = pd.read_csv(dataset, header=None)

print("The dataset is now \n {} of type {}".format(X,type(X)))

"""
METHODS, supported by a DataFrame Object
"""

print(X.info())
print(X.head(10))



"""
Do Pandas object behave like a Numpy Array ?
"""

print("The value of the {}th feature, of the {}th sample is {}".format(0,0,X[0][0]))

print("{} is of type {}".format('X',type(X)))

M = X.as_matrix()

# print(M.head(10)) Not allowed

print("{} is of type {}".format('M',type(M)))

print(X[0])

print("The first 10 features values {}".format(X[0].head(10)))

print(type(X[0]))
# ONE DIMENSIONAL OBJECT = PANDAS SERIES
# <class 'pandas.core.series.Series'>

print(type(X[0][0]))
# TWO DIMENSIONAL OBJECT = PANDAS SERIES
# <class 'numpy.float64'>

# How to get a row from a PANDAS DATA FRAME

"""
METHOD - 1
"""
#
# DeprecationWarning:
# .ix is deprecated. Please use
# .loc for label based indexing or
# .iloc for positional indexing
#

print(X.ix[0])

print(X.loc[0])

print(X.iloc[0])



"""
METHOD - 2
"""
















