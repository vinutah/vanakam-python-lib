import numpy as np

# Normal Data

arr = np.random.randn(5, 4)

# Aggregation

print(arr)
print(arr.mean())
print(arr.sum())
print(np.mean(arr))
print(arr.mean(axis=1))

