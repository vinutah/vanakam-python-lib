import numpy as np

"""
linear algebra 
    * matrix multiplication
    * decompositions
    * determinants
    * square matrix math
    * IMPORTANT PART OF ANY ARRAY LIBRARY
"""

x = np.array([[1, 2, 3], [4, 5, 6]])
y = np.array([[6, 23], [-1, 7], [8, 9]])
print(x)
print(y)
x.dot(y)
np.dot(x, np.ones(3))

from numpy.linalg import inv
from numpy.linalg import qr

X = np.random.randn(5, 5)
mat = X.T.dot(X)

print(inv(mat))

print(mat.dot(inv(mat)))

q, r = qr(mat)

print(q)
print(r)


"""
LINEAR ALGEBRA

np.diag
np.dot
np.trace
np.eig
np.det
np.inv
np.pinv
np.qr
np.svd
np.solve
np.lstsq
"""