"""
The Numpy Stack in Python

* Forms of basis for everything else
* Central object: Numpy array
* Isn't like a regular C++ or Java array
* More like a vector/matrix
* Can do addition, subtraction, multiplication, etc.
* Optimized for speed
    * Built-in matrix operations
        * Product
        * Inverse,
        * Determinant,
        * Linear systems
"""


import numpy as np

"""
CREATION
"""
# python list
l = [1, 2, 3]
# equivalent numpy array
a = np.array([1, 2, 3])

"""
LOOPING THROUGH
"""
# loop through python list
for e in l:
    print(e)
# loop through numpy array
for e in a:
    print(e)

"""
METHODS
"""

"""
NEW ELEMENTS
"""

# lets say we want to add a element
l.append(4)
print(l)

# print(a.append(4))
#
# Traceback (most recent call last):
# File "Numpy.py", line 31, in <module>
# print(a.append(4))
# AttributeError: 'numpy.ndarray' object has no attribute 'append'

l = l + [5]
# a = a + [4,5]
#
# Traceback (most recent call last):
# File "Numpy.py", line 45, in <module>
# a = a + [4,5]
# ValueError: operands could not be broadcast together with shapes (3,) (2,)
#

"""
ADDITION
"""

l2 = []
for e in l:
    l2.append(e + e)
print(l2)

# numpy + does vector addition
print(a + a)

"""
SCALING
"""

# numpy * does vector multiplication
print(2 * a)

# a python list, just gets duplicated !
print(2 * l)

# print(l**2)
#
# Traceback (most recent call last):
# File "Numpy.py", line 69, in <module>
# print(l**2)
# TypeError: unsupported operand type(s) for ** or pow(): 'list' and 'int'

# empty list create
l2 = []

# loop through this another list
for e in l:
    l2.append(e*e)
print(l2)

"""
SQUARING
"""

# think of a as a object
# all elements are powered by 2
print(a**2)

"""
MATH FUNCTIONS ON VECTORS 
"""

# other numpy methods
# all these operated element wise
print(np.sqrt(a))
print(np.log(a))
print(np.exp(a))

"""
DOT PRODUCT (Manual)
"""

# numpy array is a mathematical object
a = np.array([1, 2])
b = np.array([2, 1])

dot = 0
for e, f in zip(a, b):
    dot += e*f
print(dot)

print(a*b)
print(np.sum(a*b))
print((a*b).sum())

"""
DOT PRODUCT (FUNCTION)
"""

np.dot(a, b)
print(a.dot(b))
print(b.dot(a))

"""
ANGLE BETWEEN VECTORS
"""

print(np.sqrt((a*a).sum()))
print(np.linalg.norm(a))
cosangle = a.dot(b) / (np.linalg.norm(a) * np.linalg.norm(b))
print(cosangle)

angle = np.arccos(cosangle)
print(angle)
