import numpy as np

arr = np.arange(10)

np.save('some_array', arr)

print(np.load('some_array.npy'))

"""
saving and loading text files
"""

arr = np.loadtxt('array_ex.txt', delimiter=',')
print(arr)




